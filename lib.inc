%define SYS_EXIT 60
%define SYS_READ 0
%define SYS_WRITE 1
%define STDIN 0
%define STDOUT 1

%macro makeGlobal 1-*
%rep %0
global %1
%rotate 1
%endrep
%endmacro

section .text
;makeGlobal exit, string_length, print_string, print_char, print_newline, print_uint, print_int, string_equals, read_char, read_word, parse_uint, parse_int, string_copy
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
  .counter:
    cmp  byte [rdi+rax], 0
    je   .end
    inc  rax
    jmp  .counter
  .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    
    mov  rdx, rax ; string len
    mov  rsi, rdi ; string address
    mov  rax, SYS_WRITE ; wride number
    mov  rdi, STDOUT ; stdout
    syscall
    ret

; Принимает код символа и выводит его в stdouts
print_char:
    push rdi
    mov     rax, SYS_WRITE           ; 'write' syscall number
    mov     rdi, STDOUT           ; stdout descriptor
    mov     rsi, rsp     ; string address
    mov     rdx, 1          ; string length in bytes
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi ; number to devide
    mov r8, 10 ; num divide by
    push 0 ; 0-term
    mov rcx, 1 ; length

.devision:
    xor rdx, rdx
    div r8 ; rax - res; rdx - ost
    add rdx, '0' ; get ascii
    push rdx ; save
    inc rcx ; inc len
    test rax, rax
    jnz .devision ; not 0 - again
    
    mov r8, 0 ; index in string
.make_str:
    pop rax ; get symbol
    mov byte[digit_string + r8], al ; save to string, al - lower rax
    inc r8 ; inc index
    loop .make_str ; save all length in rcx

    mov rdi, digit_string ; string address
    jmp print_string ; printing


section .bss
digit_string: resb 24 ; reserve 24 bytes

section .text

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0 ; testing
    jge .printing ; >= 0 - print no sign
    push rdi
    mov rdi, '-'
    call print_char ; print '-'
    pop rdi
    neg rdi ; make positive
.printing:
    jmp print_uint ; print positive

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi first
; rsi second
string_equals:
    xor r8, r8 ; cur index
    xor rax, rax

.checking:
    mov al, byte[rdi+r8]
    cmp al, byte[rsi+r8]
    jne .ret_false ; char not equals - return false
    inc r8
    test rax, rax
    jnz .checking ; if not 0-symbol - go next
.ret_true:
    mov rax, 1 ; ret 1
    ret

.ret_false:
    xor rax, rax ; ret 0
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push qword 0 ; reserve stack
    mov     rax, SYS_READ ; read syscall number
    mov     rdi, STDIN ; stdin
    mov     rsi, rsp ; address
    mov     rdx, 1 ; length
    syscall
    pop rax ; get value
    ret


; Принимает: адрес начала буфера rdi, размер буфера rsi
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    enter 32, 0 ; reserve stack
    mov [rbp-8], rdi ; buf start
    mov [rbp-16], rsi ; buf size
    mov qword [rbp-32], 0 ; current index
.reading:
    call read_char ; read char

    cmp rax, ' '
    je .is_space
    cmp rax, `\t` ; if it is space  - jump
    je .is_space
    cmp rax, `\n`
    je .is_space
    test rax, rax ; endstr? - end line and exit
    jz .exit
                ; not space
    mov r9, [rbp-32] ; curr index
    mov rdi, [rbp-8] ; buf start
    mov byte [rdi+r9], al ; save symbol
    inc r9 ; inc index
    mov [rbp-32], r9 ; save index
    cmp [rbp-16], r9 ; try sub buf, must be > index to add 0-symbol
    jg .reading ; if > ok, read next

    xor rax, rax ; no - return 0
    leave
    ret

.is_space:
    mov rax, [rbp-32]
    test rax, rax ; length not 0 - word started
    jz .reading ; no - read next
.exit:
    mov rax, [rbp-8] ; buf start
    mov r9, [rbp-32] ; curr index
    mov byte [rax+r9], 0 ; add 0-symbol
    mov rdx, [rbp-32] ; length
    leave
    ret
 

; Принимает указатель на строку rdi, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax ; accumulate
    mov r8, 10 ; multiply by
    xor r9, r9 ; symbol holder
    xor r10, r10 ; power of 10 / current pos / length
.multing:
    mov r9b, byte [rdi+r10] ; get symbol
    sub r9, '0' ; try get number (must be 0-9u)
    cmp r9, 9 ; if unsigned number > 9 - endl or error
    ja .exit
    mul r8 ; mult by 10
    add rax, r9 ; place number
    inc r10 ; len++
    jmp .multing
.exit:
    mov rdx, r10
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte [rdi] ; read 1st char
    cmp rax, '-' ; test if -
    je .negative ; if yes go to negative
    call parse_uint ; no - just uint
    ret
.negative:
    inc rdi ; if neg start string from next byte
    call parse_uint ; parse mod
    neg rax ; negative
    inc rdx ; inc length
    ret 

; Принимает указатель на строку rdi, указатель на буфер rsi и длину буфера rdx
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r8, r8 ; length / offset
.copy:
    mov al, byte [rdi+r8] ; read byte
    mov byte [rsi+r8], al ; write byte
    test rax, rax ; 0-sym?
    jz .exit
    inc r8 ; inc length
    cmp rdx, r8 ; buf must be > length to save next symbol
    jg .copy ; > - ok

    xor rax, rax ; error
    ret
.exit:
    mov rax, r8 ; len
    ret
