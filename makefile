ASM=nasm
ASMFLAGS=-f elf64
LD=ld
.PHONY: all clean run_%
all: main
	echo build complete

clean:
	$(RM) *.o

run_%: %
	./$<

#ret:
#	cat temp > lib.inc

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

%: %.o
	$(LD) -o $@ $<

main.o: main.asm lib.inc
#	cat lib.inc > temp
#	cat temp | grep -o '^[a-zA-Z_]\+:' | sed 's/^/global /' > lib.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

#lib.o: lib.inc ret
#	$(ASM) $(ASMFLAGS) -o $@ $<

#main: main.o lib.o
#	$(LD) -o $@ $^

