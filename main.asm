%include "lib.inc"
section .data
message: db  'hello, world!', 10, 0
message2: db 'hello, world!', 10, 0
testnum: db '0', 100, '----hello, world!', 10, 0

section .bss
input: resb 128

section .text
global _start

_start:
  mov  rdi, 209847129842432
  call print_uint
  call print_newline
  call print_newline

  mov rdi, message
  mov rsi, message2
  call string_equals
  mov rdi, rax
  call print_uint
  call print_newline

  mov rdi, testnum
  call parse_uint
  mov rdi, rdx
  call print_int

  call print_newline
  mov rdi, input
  mov rsi, 16
  call read_word
  test rax, rax
  jz .end
  push rdx
  mov rdi, rax
  call print_string
  call print_newline
  pop rdi
  call print_uint
.end:
  xor rdi, rdi
  call exit

